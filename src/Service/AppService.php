<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

class AppService implements AppServiceInterface
{

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * AppService constructor.
     * @param Filesystem $fileSystem
     */
    public function __construct(Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
        $this->fileSystem->dumpFile('uploads/result.txt', '');
    }


    /**
     * @param bool $storeToFile
     * @return float
     */
    public function phpExecutionTime($storeToFile = false)
    {
        $startTime = microtime(true);
        for ($i=0; $i<10; $i++) {
            $aArray = array_fill(0, 1000, mt_rand(0, 1000));
            $bArray = array_fill(0, 1000, mt_rand(0, 1000));
            $cArray = [];
            foreach ($aArray as $hArray) {
                foreach ($bArray as $jArray) {
                    $cArray[] = $hArray*$jArray;
                }
            }
        }
        $time = microtime(true) - $startTime;
        if ($storeToFile == true) {
            $this->storeResultToFile('PHP execution:', $time);
        }
        return $time;
    }

    /**
     * @param bool $storeToFile
     * @return float
     */
    public function inputOutputTime($storeToFile = false)
    {
        for ($i=0; $i<100; $i++) {
            file_put_contents('text'.$i.'.txt', file_get_contents("http://www.google.com"));
        }

        $startTime = microtime(true);
        $timeAvr = 0;
        for ($i=0; $i<100; $i++) {
            $startTimeAvr = microtime(true);
            $file = fopen('text'.$i.'.txt', "r");
            fread($file, filesize('text'.$i.'.txt'));
            fclose($file);
            $timeAvr = $timeAvr + (microtime(true) - $startTimeAvr);
        }
        $time = microtime(true) - $startTime;
        if ($storeToFile == true) {
            $this->storeResultToFile('I/O time:', $time);
            $this->storeResultToFile('I/O time average:', $timeAvr/100);
        }
        return $time;
    }

    /**
     * @param bool $storeToFile
     * @return float
     */
    public function networkTime($storeToFile = false)
    {
        $startTime = microtime(true);
        for ($i=0; $i<3; $i++) {
            file_get_contents("http://www.google.com");
        }
        $time = microtime(true) - $startTime;
        if ($storeToFile == true) {
            $this->storeResultToFile('Network time:', $time);
        }
        return $time;
    }

    /**
     * Delete files
     */
    public function deleteFiles()
    {
        for ($i=0; $i<100; $i++) {
            unlink('text'.$i.'.txt');
        }
    }

    /**
     * @param string $text
     * @param float $time
     */
    private function storeResultToFile($text, $time)
    {
        $this->fileSystem->appendToFile('uploads/result.txt', $text.' '.$time.' s; ');
    }
}
