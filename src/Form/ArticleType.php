<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22/04/2018
 * Time: 23:07
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class)
            ->add('body', TextareaType::class)
                ->add(
                    'position',
                    IntegerType::class,
                    ['constraints' =>
                        [
                            new NotBlank(),
                            new Type([ 'type' => 'integer']),
                            new Regex([
                                'pattern' => '/^[0-9]\d*$/',
                                'message' => 'Please use only positive numbers.'
                            ])
                        ]
                    ]
                )
            ->add('submit', SubmitType::class);
    }
}
