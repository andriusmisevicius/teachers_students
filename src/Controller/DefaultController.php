<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Service\AppServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     * @param AppServiceInterface $service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render(
            'index.html.twig'
        );
    }

    /**
     * @Route("/login", name="auth_login")
     */
    public function login(Request $request)
    {
        return $this->render('login.html.twig');
    }

    /**
     * @Route("/admin/hello", name="admin_hello")
     */
    public function adminHello(Request $request)
    {
        return new Response("Hello admin");
    }

    /**
     * @Route("/articles", name="articles")
     */
    public function listAction()
    {
        $users = $this->getDoctrine()
                ->getRepository('App:Article')
                ->findAll();
        return $this->render('article/list.html.twig', ['articles' => $users]);
    }


    /**
     * @Route("/create-article", name="create_article")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createArticleAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($article);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('articles');
        }
        return $this->render('article/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit-article/{id}", name="edit_article")
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editArticleAction(Article $article, Request $request)
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($article);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('articles');
        }
        return $this->render('article/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete-article/{id}", name="delete_article")
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteArticleAction(Article $article)
    {
        $this->getDoctrine()->getManager()->remove($article);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('articles');
    }
}
